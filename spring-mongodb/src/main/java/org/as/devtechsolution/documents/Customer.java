package org.as.devtechsolution.documents;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
//@Document(collection = "employee")
public class Customer {
    private String id;
    private String firstName;
    private String lastName;
    private String address;
}
