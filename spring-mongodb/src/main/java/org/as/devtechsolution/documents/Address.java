package org.as.devtechsolution.documents;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Address {
    private String line1;
    private String line2;
    private String city;
    private String state;
    private int zipcode;

}
