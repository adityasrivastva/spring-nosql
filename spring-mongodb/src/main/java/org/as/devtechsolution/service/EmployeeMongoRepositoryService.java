package org.as.devtechsolution.service;

import org.as.devtechsolution.documents.Employee;
import org.as.devtechsolution.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EmployeeMongoRepositoryService {
    @Autowired
    private EmployeeRepository employeeRepository;


    public Employee save (Employee employee) {
        employee.setJoiningDate(new Date());
        return employeeRepository.insert(employee);
    }

    public List<Employee> findAll ( ) {

        return employeeRepository.findAll();
    }

    public Employee update (Employee employee) {
        return employeeRepository.save(employee);
    }

    public Employee deleteEmployee (String id) {
        Optional<Employee> byId = employeeRepository.findById(id);
        if (byId.isPresent()) {
            employeeRepository.delete(byId.get());
            return byId.get();
        }
        return null;

        //return
    }

    public Map<String, Object> findAllEmployeeInSort (int pageNo, int pageSize, String sortBy) {
        Map<String, Object> response = new HashMap<>();
        Sort sort = Sort.by(sortBy);
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        Page<Employee> employeePage = employeeRepository.findAll(pageable);
        response.put("data", employeePage.getContent());
        response.put("Total No Of Pages", employeePage.getTotalPages());
        response.put("Total No Of Elements", employeePage.getTotalElements());
        response.put("Current Page No", employeePage.getNumber());


        return response;
    }

    public List<Employee> findAllByExample (Employee employee) {

        /*
        o.s.data.mongodb.core.MongoTemplate      :
        find using query: { "firstName" : "Rama", "_class" : { "$in" : ["org.as.devtechsolution.documents.Employee"]}} fields: Document{{}}
        for class: class org.as.devtechsolution.documents.Employee in collection: employee
        */
        Example<Employee> employeeExample = Example.of(employee);

        List<Employee> allEmployee = employeeRepository.findAll(employeeExample);

        /*
        find using query: { "$or" : [{ "firstName" : { "$regex" : "^Rama$", "$options" : "i"}}],
        "_class" : { "$in" : ["org.as.devtechsolution.documents.Employee"]}} fields: Document{{}}
        for class: class org.as.devtechsolution.documents.Employee in collection: employee
        */
        /*final var matcher = ExampleMatcher.matchingAny().withIgnoreCase()
                .withMatcher("firstName", ExampleMatcher.GenericPropertyMatcher.of(ExampleMatcher.StringMatcher.EXACT));
        Example<Employee> employeeExample= Example.of(employee,matcher);

        List<Employee> all = employeeRepository.findAll(employeeExample);*/

        return allEmployee;
    }

    public List<Employee> findAllByFirstName (String firstName) {
        //return employeeRepository.findByFirstName(firstName);
        return employeeRepository.findAllByFirstNameStartingWith(firstName);//find using query: { "firstName" : { "$regularExpression" : { "pattern" : "^Ramay", "options" : ""}}} fields: Document{{}} for class: class org.as.devtechsolution.documents.Employee in collection: employee
    }

    public List<Employee> findAllByZipcode (Integer zipcode) {
        return employeeRepository.findByAddressZipcode(zipcode);
    }
/*
    public List<Employee> findBySalary (int salary) {
        return employeeRepository.findBySalary(salary);
    }

    public List<Employee> findByFirstName (String firstName) {
        return employeeRepository.findByFirstName(firstName);
    }*/
}
