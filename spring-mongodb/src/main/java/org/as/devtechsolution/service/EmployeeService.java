package org.as.devtechsolution.service;

import org.as.devtechsolution.documents.Employee;
import org.as.devtechsolution.repository.dao.EmployeeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeDAO employeeDAO;


    public Employee save (Employee employee) {
        employee.setJoiningDate(new Date());
        return  employeeDAO.save(employee);
    }

    public List<Employee> findAll ( ) {

        return employeeDAO.findAll();
    }

    public Employee update (Employee employee) {
        return  employeeDAO.save(employee);
    }

    public long deleteEmployee (String id) {
        return employeeDAO.deleteEmployee(id);
    }

    public List<Employee> findBySalary (int salary) {
        return employeeDAO.findBySalary(salary);
    }

    public List<Employee> findByFirstName (String firstName) {
        return employeeDAO.findByFirstName(firstName);
    }
}
