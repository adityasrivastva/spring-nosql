package org.as.devtechsolution.service;

import org.as.devtechsolution.documents.Customer;
import org.as.devtechsolution.repository.CustomerRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepositoy customerRepositoy;


    public List<Customer> getAll ( ) {
        return customerRepositoy.getAll();

    }


    public String add (Customer customer) {
        return  customerRepositoy.add(customer);
    }

    public String update (Customer customer) {
        return  customerRepositoy.update(customer);
    }

    public String delete ( String  id) {
        return  customerRepositoy.delete(id);
    }
}
