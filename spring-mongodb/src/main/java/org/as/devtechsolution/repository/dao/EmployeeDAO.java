package org.as.devtechsolution.repository.dao;

import org.as.devtechsolution.documents.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmployeeDAO {

    @Autowired
    private MongoTemplate mongoTemplate;

    public Employee save (Employee employee) {
        final var save = mongoTemplate.save(employee);
        return save;
    }

    public List<Employee> findAll ( ) {
        return mongoTemplate.findAll(Employee.class);
    }

    public long deleteEmployee (String id) {
        try {
            Employee byId = mongoTemplate.findById(id, Employee.class);
            return mongoTemplate.remove(byId).getDeletedCount();
        } catch (Exception e) {
            throw new RuntimeException("Employee not found with id {} "+ id);
        }

    }

    public List<Employee> findBySalary (int salary) {
        Query query= new Query(Criteria.where("salary").gte(salary));
        return  mongoTemplate.find(query, Employee.class);
    }

    public List<Employee> findByFirstName (String firstName) {
        Query query= new Query(Criteria.where("firstName").regex("^"+ firstName));
        return  mongoTemplate.find(query, Employee.class);
    }
}
