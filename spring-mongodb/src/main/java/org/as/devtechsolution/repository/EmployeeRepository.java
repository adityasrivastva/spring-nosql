package org.as.devtechsolution.repository;

import org.as.devtechsolution.documents.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String> {
    List<Employee> findByFirstName (String firstName);

    List<Employee> findAllByFirstNameStartingWith (String firstName);

    List<Employee> findByAddressZipcode (Integer zipcode);
}
