package org.as.devtechsolution.controller;

import org.as.devtechsolution.documents.Todo;
import org.as.devtechsolution.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
public class TodoController {
    @Autowired
    private TodoRepository todoRepository;

    @GetMapping("/todos")
    public ResponseEntity<?> getAllTodos(){
        List<Todo> todos = todoRepository.findAll();
        if(todos.size()>0)
            return new ResponseEntity<List<Todo>>(todos, HttpStatus.OK);
        return  new ResponseEntity<>("Not Available", HttpStatus.NOT_FOUND);

    }

    @PostMapping("/todos")
    public ResponseEntity<?> crateTodo(@RequestBody Todo todo){
        try {
            todo.setCreatedAt(new Date(System.currentTimeMillis()));
            return new ResponseEntity<>(todoRepository.save(todo), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/todos/{id}")
    public ResponseEntity<?> getTodo(@PathVariable("id") String id){
        Optional<Todo> todo = todoRepository.findById(id);
        if(todo.isPresent())
            return new ResponseEntity<Todo>(todo.get(), HttpStatus.OK);
        return  new ResponseEntity<>("Todo not found with id {}"+ id, HttpStatus.NOT_FOUND);

    }


}
