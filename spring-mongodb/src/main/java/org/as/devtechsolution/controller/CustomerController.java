package org.as.devtechsolution.controller;

import org.as.devtechsolution.documents.Customer;
import org.as.devtechsolution.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/")
    public List<Customer> getAll(){
        return customerService.getAll();
    }
    @PostMapping("/")
    public String add(@RequestBody Customer customer){
        return customerService.add(customer);
    }
    @PutMapping("/")
    public String update(@RequestBody Customer customer){
        return customerService.update(customer);
    }
    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") String id){
        return customerService.delete(id);
    }






}
