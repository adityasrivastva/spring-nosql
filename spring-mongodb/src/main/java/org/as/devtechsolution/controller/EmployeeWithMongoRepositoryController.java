package org.as.devtechsolution.controller;

import org.as.devtechsolution.documents.Employee;
import org.as.devtechsolution.service.EmployeeMongoRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/employee/mongorepository")
public class EmployeeWithMongoRepositoryController {
    @Autowired
    private EmployeeMongoRepositoryService employeeService;

    @PostMapping("/")
    public Employee createEmployee (@RequestBody Employee employee) {
        return employeeService.save(employee);
    }

    @GetMapping("/")
    public List<Employee> findAll ( ) {
        return employeeService.findAll();
    }

    @PutMapping("/")
    public Employee updateEmployee (@RequestBody Employee employee) {
        return employeeService.update(employee);
    }

    @DeleteMapping("/{id}")
    public Employee deleteEmployee (@PathVariable("id") String id) {
        return employeeService.deleteEmployee(id);
    }

    @GetMapping("/page")
    public Map<String, Object> findAllEmployeeInSort (
            @RequestParam(name = "pageno", defaultValue = "0") int pageNo,
            @RequestParam(name = "pagesize", defaultValue = "2") int pageSize,
            @RequestParam(name = "sortby", defaultValue = "id") String sortBy
    ) {
        return employeeService.findAllEmployeeInSort(pageNo, pageSize, sortBy);
    }

    @GetMapping("/example")
    public List<Employee> findAllByExample (@RequestBody Employee employee) {
        return employeeService.findAllByExample(employee);
    }

    @GetMapping("/firstName")
    public List<Employee> findAllByFirstName (@RequestParam(name = "firstName") String firstName) {
        return employeeService.findAllByFirstName(firstName);
    }

    @GetMapping("/zipcode")
    public List<Employee> findAllByZipcode (@RequestParam(name = "zipcode") Integer zipcode) {
        return employeeService.findAllByZipcode(zipcode);
    }

    /*@GetMapping("/salary")
    public List<Employee> findBySalary(@PathParam("salary") int salary){
        return employeeService.findBySalary(salary);
    }

    @GetMapping("/firstName")
    public List<Employee> findByFirstName(@PathParam("firstName") String firstName){
        return employeeService.findByFirstName(firstName);
    }*/


}
