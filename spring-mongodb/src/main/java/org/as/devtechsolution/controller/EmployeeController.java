package org.as.devtechsolution.controller;

import org.as.devtechsolution.documents.Employee;
import org.as.devtechsolution.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/")
    public Employee createEmployee(@RequestBody Employee employee){
        return employeeService.save(employee);
    }

    @GetMapping("/")
    public List<Employee> findAll(){
        return employeeService.findAll();
    }

    @PutMapping("/")
    public Employee updateEmployee(@RequestBody Employee employee){
        return employeeService.update(employee);
    }

    @DeleteMapping("/{id}")
    public long deleteEmployee(@PathVariable ("id") String id){
        return employeeService.deleteEmployee(id);
    }

    @GetMapping("/salary")
    public List<Employee> findBySalary(@PathParam("salary") int salary){
        return employeeService.findBySalary(salary);
    }

    @GetMapping("/firstName")
    public List<Employee> findByFirstName(@PathParam("firstName") String firstName){
        return employeeService.findByFirstName(firstName);
    }


}
