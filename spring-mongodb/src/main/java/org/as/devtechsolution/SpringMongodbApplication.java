package org.as.devtechsolution;

//import com.mongodb.MongoClient;
//import com.mongodb.client.MongoCollection;
//import com.mongodb.client.MongoDatabase;
//import org.bson.Document;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@SpringBootApplication
public class SpringMongodbApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringMongodbApplication.class, args);
	}

	@Bean
	public ValidatingMongoEventListener validationMongoEventListener() {
		return new ValidatingMongoEventListener(validator());
	}

	@Bean
	public LocalValidatorFactoryBean validator() {
		return new LocalValidatorFactoryBean();
	}

	@Override
	public void run (String... args) throws Exception {
		/*MongoClient mongoClient = getMongoClient();
		MongoDatabase database= mongoClient.getDatabase("test");
		MongoCollection<Document> employeeCollection = database.getCollection("customer");
		Document customer= new Document();
		customer.append("firstName", "Ram");
		customer.append("lastName","Shyam");
		customer.append("address","Ayodhya");

		employeeCollection.insertOne(customer);*/



	}

	/*public MongoClient getMongoClient(){return new MongoClient("localhost", 27017);}*/

	/*@Bean
	public MongoTemplate getMongoTemplate(){
		return new MongoTemplate();
	}*/




}
