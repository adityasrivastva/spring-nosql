package org.as.devtechsolution.service;

import org.as.devtechsolution.dao.BookMongoConfigDAO;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.ceil;

@Service
public class BookMongoConfigService {
    @Autowired
    private BookMongoConfigDAO bookMongoConfigDAO;

    public List<Object> getAllBooks ( ) {
        return bookMongoConfigDAO.getAllBooks();
    }

    public String addBook (Map<String, Object> book) {
        Document doc = new Document(book);
        return bookMongoConfigDAO.add(doc);
    }

    public String updateBook (Map<String, Object> book) {
        Document doc = new Document(book);
        String id = doc.getString("id");
        doc.remove("id");
        return bookMongoConfigDAO.update(doc, id);
    }

    public String delete (String id) {
        return bookMongoConfigDAO.delete(id);
    }

    public Map<String, Object> getAllBooksByPage (int pageNo, int pageSize, String[] fileds, String sortBy) {
        Map<String, Object> response = new HashMap<>();
        response.put("data", bookMongoConfigDAO.getAllBooksByPage(pageNo, pageSize, fileds, sortBy));
        long count = bookMongoConfigDAO.countOfElemets();
        response.put("No. of Counts", count);
        response.put("No. of Pages", ceil(count / pageSize));
        return response;

    }


    public Map<String, Object> countPage ( ) {
        Map<String, Object> response = new HashMap<>();
        response.put("Total No of Pages", bookMongoConfigDAO.countPage());

        return response;

    }

    public Map<String, Object> getByCategories (String[] categories) {
        Map<String, Object> response = new HashMap<>();
        final var byCategories = bookMongoConfigDAO.getByCategories(categories);
        response.put("data", byCategories);
        response.put("Total No. of Books", byCategories.size());

        return response;
    }
}
