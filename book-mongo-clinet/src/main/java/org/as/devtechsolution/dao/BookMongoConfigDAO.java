package org.as.devtechsolution.dao;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BookMongoConfigDAO {

    private MongoClient client;

    public MongoClient getClient ( ) {
        if (client == null)
            client = new MongoClient("localhost", 27017);
        return client;

    }

    public List<Object> getAllBooks ( ) {
        final MongoCollection<Document> booksCollection = getDocumentMongoCollection("books");
        final var documents = booksCollection.find();
        List<Object> booksResponse = new ArrayList<>();
        for (Document doc : documents) {
            booksResponse.add(doc);

        }
        return booksResponse;
    }


    private MongoCollection<Document> getDocumentMongoCollection (String CollectionName) {
        final var mongoClient = getClient();
        final var database = mongoClient.getDatabase("test");
        final var collection = database.getCollection(CollectionName);
        return collection;
    }

    public String add (Document doc) {
        final var booksCollection = getDocumentMongoCollection("books");
        String response = "Successfully Inserted";
        try {
            booksCollection.insertOne(doc);
        } catch (Exception e) {
            e.printStackTrace();
            response = "Successfully Not Inserted";
        }

        return response;
    }

    public String update (Document doc, String id) {

        final var booksCollection = getDocumentMongoCollection("books");
        BasicDBObject filter = new BasicDBObject("_id", id);
        BasicDBObject update = new BasicDBObject("$set", doc);
        String response = "Successfully Updated";
        try {
            booksCollection.updateOne(filter, update);
            response = "Successfully Updated!!!!!";
        } catch (Exception e) {
            e.printStackTrace();
            response = "Successfully Not Updated";
        }

        return response;
    }

    public String delete (String id) {

        final var booksCollection = getDocumentMongoCollection("books");
        BasicDBObject filter = new BasicDBObject("_id", id);
        String response = "Successfully Deleted";
        try {
            booksCollection.deleteOne(filter);
            response = "Successfully Deleted!!!!!";
        } catch (Exception e) {
            e.printStackTrace();
            response = "Successfully Not Deleted";
        }
        return response;
    }

    public List<Object> getAllBooksByPage (int pageNo, int pageSize, String[] fields, String sortBy) {
        final var booksCollection = getDocumentMongoCollection("books");

        BasicDBObject projection = new BasicDBObject("_id", 0);// to remove id
        for (String field : fields) {
            projection.append(field, 1);

        }
        BasicDBObject sort = new BasicDBObject(sortBy, 1);
        final var documents = booksCollection
                .find()
                .projection(projection)// {'title':1,'pageCount':1}
                .sort(sort)// {'pageCount':-1}
                .skip(pageNo * pageSize)//10
                .limit(pageSize);// 10

        List<Object> booksResponse = new ArrayList<>();
        for (Document doc : documents) {
            booksResponse.add(doc);

        }
        return booksResponse;


    }

    public long countOfElemets ( ) {
        final var booksCollection = getDocumentMongoCollection("books");

        return booksCollection.countDocuments();
    }

    public String countPage ( ) {
        final var booksCollection = getDocumentMongoCollection("books");
            /* $group: {
                          _id: null,
                          count: {
                            $sum : "$pageCount"
                          }
                    }
            */
        BasicDBObject sum = new BasicDBObject("$sum", "$pageCount");
        BasicDBObject grp = new BasicDBObject();
        grp.append("_id", null);
        grp.append("count", sum);
        BasicDBObject group = new BasicDBObject("$group", grp);
        System.out.println(group.toJson());
        List<BasicDBObject> pipelines = new ArrayList<BasicDBObject>();
        pipelines.add(grp);
        final var aggregate = booksCollection.aggregate(pipelines);


        return aggregate.first().get("count").toString();
    }

    public List<Object> getByCategories (String[] categories) {
        // {'categories': {$elemMatch: {$in:['Mobile', 'Java']}}}

        final var booksCollection = getDocumentMongoCollection("books");
        BasicDBObject in = new BasicDBObject("$in", categories);
        BasicDBObject elemMatch = new BasicDBObject("$elemMatch", in);
        BasicDBObject category = new BasicDBObject("categories", elemMatch);

        BasicDBObject projection = new BasicDBObject();
        projection.append("title", 1);
        projection.append("_id", 0);
        projection.append("categories", 1);
        final var documents = booksCollection.find(category)
                .projection(projection);

        List<Object> booksResponse = new ArrayList<>();
        for (Document doc : documents) {
            booksResponse.add(doc);

        }
        return booksResponse;
    }


}
