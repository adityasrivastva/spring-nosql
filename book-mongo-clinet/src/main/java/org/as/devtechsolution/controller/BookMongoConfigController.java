package org.as.devtechsolution.controller;

import org.as.devtechsolution.service.BookMongoConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/mongo-config/books")
public class BookMongoConfigController {

    @Autowired
    private BookMongoConfigService bookMongoConfigService;

    @GetMapping("/")
    public ResponseEntity<List<Object>> getAllBooks ( ) {
        return new ResponseEntity<>(bookMongoConfigService.getAllBooks(), HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<String> addBook (@RequestBody Map<String, Object> book) {
        return new ResponseEntity<>(bookMongoConfigService.addBook(book), HttpStatus.OK);
    }

    @PutMapping("/")
    public ResponseEntity<String> updateBook (@RequestBody Map<String, Object> book) {
        return new ResponseEntity<>(bookMongoConfigService.updateBook(book), HttpStatus.OK);
    }

    @DeleteMapping("/")
    public ResponseEntity<String> deleteBook (@RequestParam String id) {
        return new ResponseEntity<>(bookMongoConfigService.delete(id), HttpStatus.OK);
    }

    @GetMapping("/page")
    public ResponseEntity<Map<String, Object>> getAllBooksByPage (
            @RequestParam(name = "pageno", defaultValue = "0") int pageNo,
            @RequestParam(name = "pagesize", defaultValue = "2") int pageSize,
            @RequestParam(name = "fields", defaultValue = "title,pageCount") String[] fileds,
            @RequestParam(name = "sortby", defaultValue = "id") String sortBy
    ) {
        return new ResponseEntity<>(bookMongoConfigService.getAllBooksByPage(pageNo, pageSize, fileds, sortBy), HttpStatus.OK);
    }

    @GetMapping("/countpage")
    public ResponseEntity<Map<String, Object>> countPage ( ) {
        return new ResponseEntity<>(bookMongoConfigService.countPage(), HttpStatus.OK);
    }

    @GetMapping("/categories")
    public ResponseEntity<Map<String, Object>> getByCategories (@RequestParam(value = "category", required = true) String[] categories) {
        return new ResponseEntity<>(bookMongoConfigService.getByCategories(categories), HttpStatus.OK);
    }

}
